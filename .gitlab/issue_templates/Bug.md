In order to submit an effective bug report, please include the following information along with your issue description.

### Environment Details

> Please share the following basic details about your setup.

* Foundry VTT Version: (Example 0.5.4)
* Turn Marker Version: (Example 2.2.0)

### Issue Description

> Please provide a description of your issue below. Including console error messages, screenshots or videos can be particularly helpful.

/label ~Bug
